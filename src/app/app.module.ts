import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Router} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { StudentComponentComponent } from './components/student-component/student-component.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { StudentPersonalComponent } from './student-personal/student-personal.component';

const appRoutes = [
  {path: 'students', component: StudentComponentComponent},
  {path: 'students/editStudent', component: StudentPersonalComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    StudentComponentComponent,
    StudentPersonalComponent
  ],
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    NgbModule.forRoot(),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
