import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {StudentPersonalComponent} from '../../student-personal/student-personal.component';

@Component({
  selector: 'app-student-component',
  templateUrl: './student-component.component.html',
  styleUrls: ['./student-component.component.css']
})
export class StudentComponentComponent implements OnInit {

  private serverUrl: 'http://localhost:8080/dziekanat/students/courseAndGpaView';
  students: Student[];
  student: Student;

  constructor(private http: HttpClient, private router: Router) {
  }
  @Output() myEvent = new EventEmitter();
  ngOnInit() {
    this.http.get<Student[]>('http://localhost:8080/dziekanat/students/courseAndGpaView').subscribe(
      ((results: Student[]) => this.students = results)
    );
    this.student = new Student();
    this.student.personalInfo = new PersonalInfo();
    this.student.averageGrades = new Array();
  }

  // goToEdit() {
  //   this.router.navigate(['/students/editStudent']);
  // }

  deleteStudent(studentId: number) {
    this.http.delete('http://localhost:8080/dziekanat/students/' + studentId.toString()).subscribe(
      ((results: Student[]) => this.students = results)
    );
  }

  addStudent(newStudent: Student) {
    this.student = newStudent;
    this.http.post('http://localhost:8080/dziekanat/students', this.student).subscribe(
      ((results: Student[]) => this.students = results)
    );
  }
}

export class Student {
  id: number;
  personalInfo = new PersonalInfo();
  bookNumber: number;
  averageGrades: Gpa[];
}

class PersonalInfo {
  firstName: string;
  lastName: string;
  birthDate: Date;
  man: boolean;
}

class Gpa {
  yearOfGpa: string;
  gpaValue: number;
}

